const MakeMJPEGReader = function(imgID, srcURL, advParam = null) {
    const WSMJPEGReader = {};

    // Socket listener with autoreconnect
    const SASVideoConnection = function(socURL, protocol = "rtp") {
        const retStruct = {};
        // Get play targe -> skip rest if null
        retStruct.videoHTMLElem = document.getElementById(imgID);
        if(!retStruct.videoHTMLElem) {
            console.error(`${imgID} not exist on page!`);
            return null;
        }
        retStruct.urlConnection = socURL;
        retStruct.protocol = protocol;
        retStruct.sasSocket = null;
        // To not recreate reader each time -> (this one use least time and not crash big sizes)
        retStruct.hexToBase64 = byteArr => btoa(new Uint8Array(byteArr).reduce((data, byte) => data + String.fromCharCode(byte), ''));
         // Declare handling functions as can be call right after creation
        retStruct.wsFrameReceived = (function(msg){
            this.videoHTMLElem.src = 'data:image/jpeg;base64,' + this.hexToBase64(msg.data);
        }).bind(retStruct);
        retStruct.wsSocketOpened = (function(){
            this.reconnTimeout = 1;
        }).bind(retStruct);
        retStruct.reconnTimeout = 1;
        retStruct.wsOnCloseError = (function(clMsg){
            // On close -> we reconnect in 1 to 4 seconds
            this.sasSocket = null;// Assume closed
            setTimeout((function(){
                this.connectSocket();
            }).bind(this), this.reconnTimeout * 1000);
            if(this.reconnTimeout < 4)this.reconnTimeout *= 2;
        }).bind(retStruct);
        retStruct.connectSocket = (function(){
            if(!this.sasSocket || this.sasSocket == null) {
                this.sasSocket = new WebSocket(this.urlConnection, this.protocol || null);// Send RTP protocol, buuut ignored for now
                this.sasSocket.binaryType = "arraybuffer";// reduce convertion processor time
                this.sasSocket.onmessage = this.wsFrameReceived;
                this.sasSocket.onopen = this.wsSocketOpened;
                this.sasSocket.onclose = this.wsOnCloseError;
                this.sasSocket.onwrror = this.wsOnCloseError;
            }
            else {// Socket already opened -> skip for now
    
            }
        }).bind(retStruct);

        return retStruct;
    }

    // Connection to Audio stream
    const SASAudioConnection = function(socURL, protocol = "rtp") {
        const AUDConReturn = {}

        // Create Audio socket
        if((AUDConReturn.socketConnection = SASVideoConnection(socURL)) == null) {
            return null;
        }
        
        // Apply Header to Audio Chunk
        AUDConReturn.withWaveHeader = (data, numberOfChannels, sampleRate) => {
            const header = new ArrayBuffer(44);
          
            const d = new DataView(header);
          
            d.setUint8(0, "R".charCodeAt(0));
            d.setUint8(1, "I".charCodeAt(0));
            d.setUint8(2, "F".charCodeAt(0));
            d.setUint8(3, "F".charCodeAt(0));
          
            d.setUint32(4, data.byteLength / 2 + 44, true);
          
            d.setUint8(8, "W".charCodeAt(0));
            d.setUint8(9, "A".charCodeAt(0));
            d.setUint8(10, "V".charCodeAt(0));
            d.setUint8(11, "E".charCodeAt(0));
            d.setUint8(12, "f".charCodeAt(0));
            d.setUint8(13, "m".charCodeAt(0));
            d.setUint8(14, "t".charCodeAt(0));
            d.setUint8(15, " ".charCodeAt(0));
          
            d.setUint32(16, 16, true);
            d.setUint16(20, 1, true);
            d.setUint16(22, numberOfChannels, true);
            d.setUint32(24, sampleRate, true);
            d.setUint32(28, sampleRate * 1 * 2);
            d.setUint16(32, numberOfChannels * 2);
            d.setUint16(34, 16, true);
          
            d.setUint8(36, "d".charCodeAt(0));
            d.setUint8(37, "a".charCodeAt(0));
            d.setUint8(38, "t".charCodeAt(0));
            d.setUint8(39, "a".charCodeAt(0));
            d.setUint32(40, data.byteLength, true);
          
            return ((buffer1, buffer2) => {
                const tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
              
                tmp.set(new Uint8Array(buffer1), 0);
                tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
              
                return tmp.buffer;
            })(header, data);
        };
        
        // Initial timing to start play
        AUDConReturn.chunkStartTime = 0;

        // Redefine chunk handling function
        AUDConReturn.socketConnection.wsFrameReceived = (function(msg){
            if (this.soundPlayerContext.state !== 'running')return;
            this.soundPlayerContext.decodeAudioData(this.withWaveHeader(msg.data, 2, 48000)).then((function(decBuffer){
                sourceBuf = this.soundPlayerContext.createBufferSource();
                sourceBuf.buffer = decBuffer;
                sourceBuf.connect(this.soundPlayerContext.destination);
                if(this.chunkStartTime == 0 || this.soundPlayerContext.currentTime > this.chunkStartTime) {// If we dropped some buffers OR start
                    this.chunkStartTime = this.soundPlayerContext.currentTime + decBuffer.duration / 2;
                }
                sourceBuf.start(this.chunkStartTime);
                this.chunkStartTime += decBuffer.duration;
            }).bind(AUDConReturn)).catch((function(exMsg){
                //console.log(exMsg)
            }).bind(AUDConReturn));
        }).bind(AUDConReturn);
        
        // Get Audio target context - and not create multiple context on multiple Readers
        AUDConReturn.soundPlayerContext = window.wsmjpgAudioContext || new(window.AudioContext || window.webkitAudioContext)();
        window.wsmjpgAudioContext = AUDConReturn.soundPlayerContext;
        
        // Check if got start element
        AUDConReturn.audPlayElem = document.getElementById(advParam.audioStartButton);
        if(!AUDConReturn.audPlayElem) {
            console.warn(`${advParam.audioStartButton} not exist on page! Link on mousedown event!`);
            document.documentElement.addEventListener("mousedown", (function(){
                if (this.soundPlayerContext.state !== 'running') this.soundPlayerContext.resume();
            }).bind(AUDConReturn))
        }
        else {
        }

        AUDConReturn.connectSocket = AUDConReturn.socketConnection.connectSocket;

        return AUDConReturn;
    }

    // Create video handling socket
    if((WSMJPEGReader.videoSConn = SASVideoConnection(srcURL)) == null) {
        console.error(`videoSConn not created!`);
        return null;
    }
    WSMJPEGReader.videoSConn.connectSocket();// Attempt connection

    // Check if advance parameters
    if(advParam && advParam != null && advParam.audioURL && advParam.audioURL != null) {
        if((WSMJPEGReader.audioSConn = SASAudioConnection(advParam.audioURL)) == null) {
            console.error(`audioSCon not created!`);
            return null;
        }
        // Try launch socket
        WSMJPEGReader.audioSConn.connectSocket();        
    }

    return WSMJPEGReader;
}