package services

import (
	"fmt"
	"os/exec"
	"sastreamer/structs"
	"strconv"
	"strings"
)

func GetCamList() (*structs.CameraContext, error) {
	retCameras := structs.CameraContext{CamListHeader: `Camera's devices`}
	lsCommand := exec.Command("ls", "-f", "-1", "/dev")
	cmOUT, cmErr := lsCommand.Output()
	if cmErr != nil {
		return nil, cmErr
	}
	unfilteredDevices := strings.Split(string(cmOUT), "\n")
	retCameras.CameraDevices = []string{}
	for _, siDev := range unfilteredDevices {
		if strings.Contains(siDev, "video") {
			retCameras.CameraDevices = append(retCameras.CameraDevices, fmt.Sprintf("/dev/%s", siDev))
		}
	}
	return &retCameras, nil
}

func getRunningGSTWS() ([]string, error) {
	getProcList := exec.Command("ps", "-e", "-f")
	cmOUT, cmErr := getProcList.Output()
	if cmErr != nil {
		return nil, cmErr
	}
	unfiltProcess := strings.Split(string(cmOUT), "\n")
	retStrList := []string{}
	for _, siProc := range unfiltProcess {
		if !strings.Contains(siProc, "Killed") {
			retStrList = append(retStrList, siProc)
		}
	}
	return retStrList, nil
}

func getParamValue(fullStr string, paramName string) (string, int) {
	seekIDX := strings.Index(fullStr, paramName)
	if seekIDX == -1 {
		return "", -1
	}
	nextParIDX := strings.Index(fullStr[seekIDX+1:], " /")
	if nextParIDX == -1 {
		nextParIDX = len(fullStr) - 1
	} else {
		nextParIDX = seekIDX + nextParIDX
	}
	return fullStr[seekIDX+len(paramName) : nextParIDX+1], seekIDX
}

func GetStreamsList() ([]*structs.StreamPipeInfo, error) {
	processStrings, gErr := getRunningGSTWS()
	if gErr != nil {
		return nil, gErr
	}
	retStreams := []*structs.StreamPipeInfo{}
	for _, siStr := range processStrings {
		if !strings.Contains(siStr, "gst-to-ws") {
			continue
		}
		pipeString, testIDX := getParamValue(siStr, "/pipe ")
		if testIDX == -1 {
			pipeString = "ERROR in seeking PIPE"
		}
		hostString, testIDX := getParamValue(siStr, "/h ")
		if testIDX == -1 {
			hostString = "ERROR"
		}
		portString, testIDX := getParamValue(siStr, "/p ")
		if testIDX == -1 {
			portString = "0"
		}
		portID, gErr := strconv.Atoi(portString)
		if gErr != nil {
			portID = 0
		}
		procPID, gErr := strconv.Atoi(siStr[9:14])
		if gErr != nil {
			procPID = 0
		}
		retStreams = append(retStreams, &structs.StreamPipeInfo{PipeGSConstructor: pipeString, SourceHost: hostString, SourcePort: uint16(portID), PIDProcess: procPID})
	}
	return retStreams, nil
}
