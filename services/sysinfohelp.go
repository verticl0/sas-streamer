package services

import (
	"net"
)

func GetOutboundIP() net.IP {
	// UDP connection works offline TOO
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil { // but for ERROR -> no HARD FAULT required -> return 127.0.0.1
		defer conn.Close() // Cant defer before error chack ? needed
		return net.ParseIP("127.0.0.1")
	}
	defer conn.Close()
	// Return OUT IP if found
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return localAddr.IP
}
