package httphandlers

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func GetJScriptDispatcher(rootJSDir string, fileServer http.Handler) http.Handler {
	return http.HandlerFunc(func(rWrt http.ResponseWriter, rqH *http.Request) {
		// here we can do any kind of checking, in this case we'll just split the url and
		// check if the image name is in the allowedImages map, we can check in a DB or something
		parts := strings.Split(rqH.URL.Path, "/")
		jsName := parts[len(parts)-1]

		if _, fileErr := os.Stat(filepath.Join(rootJSDir, jsName)); fileErr == nil {
			rqH.RequestURI = "/" + jsName
			fileServer.ServeHTTP(rWrt, rqH) // StripPrefix() and FileServer() return a Handler that implements ServerHTTP()
		} else {
			http.NotFound(rWrt, rqH)
		}
	})
}
