package httphandlers

import (
	"fmt"
	"html/template"
	"net/http"
	"sastreamer/services"
	"sastreamer/structs"
)

func GetNamedHTML(relFileName string) http.Handler {
	return http.HandlerFunc(func(rWrt http.ResponseWriter, rqH *http.Request) {
		t := template.Must(template.ParseFiles(relFileName))
		t.Execute(rWrt, nil)
	})
}

func GetCamListHTML(camListFileName string) http.Handler {
	return http.HandlerFunc(func(rWrt http.ResponseWriter, rqH *http.Request) {
		cameraList, sErr := services.GetCamList()
		if sErr != nil {
			cameraList = &structs.CameraContext{CamListHeader: fmt.Sprintf("Error : \"%s\"", sErr.Error()), CameraDevices: []string{}}
		}
		streamsList, sErr := services.GetStreamsList()
		if sErr != nil {
			streamsList = []*structs.StreamPipeInfo{}
		}
		devListTmpl, _ := template.ParseFiles(camListFileName)
		devListTmpl.Execute(rWrt, struct {
			CAM    *structs.CameraContext
			STREAM []*structs.StreamPipeInfo
		}{cameraList, streamsList})
	})
}

func GetTestMJPEGHTML(testMjpegFileName string) http.Handler {
	return http.HandlerFunc(func(rWrt http.ResponseWriter, rqH *http.Request) {
		devListTmpl, _ := template.ParseFiles(testMjpegFileName)
		locIP := services.GetOutboundIP().String()
		devListTmpl.Execute(rWrt, struct {
			VideoADR   string
			AudioADR   string
			ExpoCamADR string
		}{locIP + ":5010", locIP + ":5011", locIP + ":5012"}) // Port hardcoded for now -> from settings later
	})
}
