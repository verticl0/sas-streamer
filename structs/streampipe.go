package structs

type StreamPipeInfo struct {
	PipeGSConstructor string
	SourceHost        string
	SourcePort        uint16
	PIDProcess        int
}
