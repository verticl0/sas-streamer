package structs

type CameraContext struct {
	CamListHeader string
	CameraDevices []string
}
