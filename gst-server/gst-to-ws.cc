#include "wsstreamserver.h"
#include <gst/gst.h>
#include <signal.h>
#include <string.h>
#ifndef MinGW_COMPILE
  #include <thread>
  #include <chrono>
#endif

// Structure contains OUR pipeline INFO
#define CONST_BUF_SIZE 2 * 1024 * 1024
// Assume ther can be no more than 2Mb frame
typedef struct {
  GstElement *pipeSAS;
  GstElement *sinkWebSocketServer;
  GMainLoop *gstLoop;
  GstBus *busPipe;
  wsstreamserver::stream_ws_server *sasStreamServer;
  uint8_t constBuffer[CONST_BUF_SIZE];
} SASPipe;

// Handle GS error/other messages
static gboolean handle_message (GstBus *bus, GstMessage *msg, gpointer udata) {
  GError *err;
  gchar *debug_info;
  SASPipe *iPData = (SASPipe*)udata;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_ERROR:
      gst_message_parse_error (msg, &err, &debug_info);
      gst_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
      gst_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
      g_clear_error (&err);
      g_free (debug_info);
      g_main_loop_quit (iPData->gstLoop);
      break;
    case GST_MESSAGE_EOS:
      gst_print ("End-Of-Stream reached.\n");
      g_main_loop_quit (iPData->gstLoop);
      break;
    case GST_MESSAGE_WARNING:
      gst_message_parse_warning (msg, &err, &debug_info);
      gst_println ("Element %s: %s", GST_OBJECT_NAME (msg->src), err->message);
      gst_println ("Debugging information: %s", debug_info ? debug_info : "none");
      g_clear_error(&err);
      g_free(debug_info);
      break;
    case GST_MESSAGE_STATE_CHANGED: {
      GstState old_state, new_state, pending_state;
      gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
    } break;
  }

  return true;
}

static GstFlowReturn new_frame_handler (GstElement *appsink, gpointer udata) {
  /* Retrieve the buffer */
  GstSample *sample = NULL;
  GstBuffer *buffer = NULL;
  SASPipe *iPData = (SASPipe*)udata;
  int32_t iGotSize;
  g_signal_emit_by_name (appsink, "pull-sample", &sample);
  if (sample) {
    buffer = gst_sample_get_buffer(sample);
    if (buffer) {
      iGotSize = gst_buffer_extract(buffer, 0, iPData->constBuffer, CONST_BUF_SIZE);
      iPData->sasStreamServer->postBufferOnClients(iPData->constBuffer, iGotSize); // Send buffer to destinations
    }
    gst_sample_unref (sample);
    return GST_FLOW_OK;
  }
  return GST_FLOW_ERROR;
}

#define GST_PIPE_GENERATION_STRING_SIZE 1024
int main(int argc, char **argv) {
  int iDevPos = -1, iIPPos = -1, iPortPos = -1, iGSTOpt = -1, iTemp;
  bool fErrFlag = false;
  gchar pipeString[GST_PIPE_GENERATION_STRING_SIZE];
  SASPipe sasPipeContext;
  GstStateChangeReturn rtState;
  wsstreamserver::serverSettingsContext settings;

  // Check if got devSource IP:Port
  for(int iAN = 0; iAN < argc; iAN++) {
    if(strcmp(argv[iAN], "/pipe") == 0) {// device name
      iDevPos = iAN + 1;
    }
    if(strcmp(argv[iAN], "/h") == 0) {// HOST IP
      iIPPos = iAN + 1;
    }
    if(strcmp(argv[iAN], "/p") == 0) {// HOST Port
      iPortPos = iAN + 1;
    }
    if(strcmp(argv[iAN], "/appsink") == 0) {// GST parameters
      iGSTOpt = iAN + 1;
    }
    if(strcmp(argv[iAN], "/?") == 0) {// Call for Help -> type and exit
      gst_print("/pipe - set stream source pipeline '/pipe v4l2src ! mpegtsmux' <required>\n");
      gst_print("/h - set server listen host as '/h 192.168.50.132' <required>\n");
      gst_print("/p - set server listen port as '/p 5010'\n");
      gst_print("/appsink - set appsink element parameters 'max-buffers=3 drop=true emit-signals=true sync=false'\n");
      gst_print("IF no info has and app stopping - export GST_DEBUG=2 - and rerun for see WARNING messages\n");
      gst_print("\n");
      return 0;
    }
  }
  if(iDevPos == -1){fErrFlag = true; gst_printerr("No pipeline specifyied /pipe\n");}
  if(iPortPos == -1){fErrFlag = true; gst_printerr("No HOST PORT specifyied /p\n");}
  if(fErrFlag){gst_print("Call with /? for help.\n"); return -1;}// NEEDED info for setup stream
  
  /* Initialize GStreamer */
  gst_print("Starting GST\n");
  gst_init (&argc, &argv);// TODO : rearrange init vars

  /* Create the elements */
  gst_print("Creating pipeline\n");
  iTemp = g_snprintf(pipeString, GST_PIPE_GENERATION_STRING_SIZE, "%s ! appsink name=ws-vide-sink %s", argv[iDevPos], iGSTOpt != -1 ? argv[iGSTOpt] : "max-buffers=10 drop=true emit-signals=true sync=false");
  if(iTemp >= GST_PIPE_GENERATION_STRING_SIZE) {
    gst_printerrln("Too long pipeline string!");
    return -11;
  }
  else gst_println("Full GST launch - '%s'", pipeString);
  sasPipeContext.pipeSAS = gst_parse_launch(pipeString, NULL);
  if(sasPipeContext.pipeSAS == NULL) { // Handler Error
    gst_println("Some error in pipe creation!");
    return -10;
  }

  // Create watch handler
  sasPipeContext.gstLoop = g_main_loop_new (NULL, false);
  sasPipeContext.busPipe = gst_element_get_bus (sasPipeContext.pipeSAS);
  gst_bus_add_watch (sasPipeContext.busPipe, handle_message, &sasPipeContext);

  // OUR videosink (audio maybe later)
  /* set to PAUSED to make the first frame arrive in the sink */
  sasPipeContext.sinkWebSocketServer = gst_bin_get_by_name (GST_BIN (sasPipeContext.pipeSAS), "ws-vide-sink");
  g_signal_connect(sasPipeContext.sinkWebSocketServer, "new-sample", G_CALLBACK (new_frame_handler), &sasPipeContext);

  /* Start playing */
  rtState = gst_element_set_state (sasPipeContext.pipeSAS, GST_STATE_PLAYING);
  if (rtState == GST_STATE_CHANGE_FAILURE) {
    gst_printerr ("Unable to set the pipeline to the playing state.\n");
    gst_object_unref (sasPipeContext.pipeSAS);
    return -3;
  }

  // Creating WSServer 
  sasPipeContext.sasStreamServer = new wsstreamserver::stream_ws_server(atoi(argv[iPortPos]), iIPPos == -1 ? NULL : argv[iIPPos]);
  settings.SendTCPStack = 300000;// 300kBytes -> buffering size
  sasPipeContext.sasStreamServer->initServer(&settings);
  sasPipeContext.sasStreamServer->startServer(false);

  /* Run GST Loop */
  g_main_loop_run (sasPipeContext.gstLoop);

  sasPipeContext.sasStreamServer->stopServer(true);
  delete sasPipeContext.sasStreamServer;

  /* Free resources */
  g_main_loop_unref (sasPipeContext.gstLoop);
  gst_object_unref (sasPipeContext.busPipe);
  gst_element_set_state (sasPipeContext.pipeSAS, GST_STATE_NULL);
  gst_object_unref (sasPipeContext.pipeSAS);
  return 0;
}