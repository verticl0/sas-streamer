/*
 * This file is part of wSocketStream.
 *
 * Developed for the LSST Data Management System.
 * This product includes software developed by the LSST Project
 * (https://www.lsst.org).
 * See the COPYRIGHT file at the top-level directory of this distribution
 * for details of code ownership.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <map>
#include <list>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <sys/poll.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

// Define externally or global __WSH_DEBUG_LVL__
#ifndef __WSH_DEBUG_LVL__
#define __WSH_DEBUG_LVL__ 4
#endif

// Time printing script
#define print_time_script(xcolorcode) {             \
    time_t timer;                                   \
    char buffer[28];                                \
    struct tm *tm_info;                             \
    timer = time(NULL);                             \
    tm_info = localtime(&timer);                    \
    strftime(buffer, 28, "[%H:%M:%S] : ", tm_info); \
    printf("%s", buffer);                           \
    printf(xcolorcode);                             \
}

// Just empty code if no enough level
#if __WSH_DEBUG_LVL__ >= 1
#define msg_dbg_error(xerr) {                           \
        print_time_script("\033[0;31m")                 \
        printf xerr;                                    \
        printf("\033[0m\n");                            \
    }
#else
#define msg_dbg_error(xerr) {}
#endif
#if __WSH_DEBUG_LVL__ >= 2
#define msg_dbg_warning(xwrn) {                         \
        print_time_script("\033[0;33m")                 \
        printf xwrn;                                    \
        printf("\033[0m\n");                            \
    }
#else
#define msg_dbg_warning(xwrn) {}
#endif
#if __WSH_DEBUG_LVL__ >= 3
#define msg_dbg_info(xinf) {                            \
        print_time_script("\033[0;32m")                 \
        printf xinf;                                    \
        printf("\033[0m\n");                            \
    }
#else
#define msg_dbg_info(xinf) {}
#endif
#if __WSH_DEBUG_LVL__ >= 4
#define msg_dbg_call(xcinf) {                           \
        print_time_script("\033[0;34m")                 \
        printf(__FUNCTION__);                           \
        printf(" called - ");                           \
        printf xcinf;                                   \
        printf("\033[0m\n");                            \
    }
#else
#define msg_dbg_call(xinf) {}
#endif

namespace wsstreamserver {
    class wsutility {
    public:
        static const uint8_t WS_OPCODE_CONTFRAME = 0;
        static const uint8_t WS_OPCODE_TEXTFRAME = 1;
        static const uint8_t WS_OPCODE_BINFRAME = 2;
        static const uint8_t WS_OPCODE_CONCLOSEFRAME = 8;
        static const uint8_t WS_OPCODE_PINGFRAME = 9;
        static const uint8_t WS_OPCODE_PONGFRAME = 10;
        static const uint8_t WS_FINFRAME = 0x80;
    public:
        /**
         * Get local IP address.
         *
         * @param ipAddrStorage Pointer to buffer to fill with ardress (size INET_ADDRSTRLEN)
         * @return 0 if IP defined. less zero oterwise.
         */
        static int32_t getLocalIPAddr(char *ipAddrStorage) {
            msg_dbg_call((""));

            // Create UDP socket to assign adress to US
            int sock = socket(PF_INET, SOCK_DGRAM, 0);
            sockaddr_in loopback;
            socklen_t addrlen = sizeof(loopback);

            if (sock == -1) { // Socket creation failed
                msg_dbg_error(("Failed to create socket for define local IP!"));
                return -1;
            }

            // Use random port and target IP
            loopback.sin_family = AF_INET;
            loopback.sin_addr.s_addr = 1337; // can be any IP address
            loopback.sin_port = htons(9);    // using debug port

            // Try "connect" as it UDP no real connection required
            if (connect(sock, reinterpret_cast<sockaddr *>(&loopback), sizeof(loopback)) == -1) {
                close(sock);
                msg_dbg_error(("Socket for define local IP failed to connect!"));
                return -2;
            }

            // Get received address
            if (getsockname(sock, reinterpret_cast<sockaddr *>(&loopback), &addrlen) == -1) {
                close(sock);
                msg_dbg_error(("Could not getsockname of local IP socket!"));
                return -3;
            }
            close(sock); // Here we need socket no more

            // Convert to adress string and exit
            if (inet_ntop(AF_INET, &loopback.sin_addr, ipAddrStorage, INET_ADDRSTRLEN) == 0x0) {
                msg_dbg_error(("Failed to translate IP to string!"));
                return -4;
            }
            else {
                msg_dbg_info(("Local ip address: %s", ipAddrStorage));
                return 0;
            }
        }
        /**
         * Function specificaly used in sha1base64
         */
        static uint32_t rol(uint32_t value, uint32_t bits) { return (value << bits) | (value >> (32 - bits)); }
        /**
         * Create Base64 response to IN
         * Warning: in MUST have 64 bytes more than in_len
         * 
         * @param in Buffer containing input string (in_len + 64 sized)
         * @param in_len Size of input message
         * @param out Output buffer to contain response message
         * @return Size of response message (28 always)
         */
        static uint32_t sha1base64(uint8_t* in, uint64_t in_len, char* out) {
            uint32_t h0[5] = {0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0};
            uint64_t total_len = in_len;
            in[total_len++] = 0x80;
            int padding_size = (64 - (total_len + 8) % 64) % 64;
            while (padding_size--) in[total_len++] = 0;
            for (uint64_t i = 0; i < total_len; i += 4) {
                uint32_t& w = *(uint32_t*)(in + i);
                w = be32toh(w);
            }
            *(uint32_t*)(in + total_len) = (uint32_t)(in_len >> 29);
            *(uint32_t*)(in + total_len + 4) = (uint32_t)(in_len << 3);
            for (uint8_t* in_end = in + total_len + 8; in < in_end; in += 64) {
                uint32_t* w = (uint32_t*)in;
                uint32_t h[5];
                memcpy(h, h0, sizeof(h));
                for (uint32_t i = 0, j = 0; i < 80; i++, j += 4) {
                    uint32_t &a = h[j % 5], &b = h[(j + 1) % 5], &c = h[(j + 2) % 5], &d = h[(j + 3) % 5], &e = h[(j + 4) % 5];
                    if (i >= 16) w[i & 15] = rol(w[(i + 13) & 15] ^ w[(i + 8) & 15] ^ w[(i + 2) & 15] ^ w[i & 15], 1);
                    if (i < 40) {
                    if (i < 20)
                        e += ((b & (c ^ d)) ^ d) + 0x5A827999;
                    else
                        e += (b ^ c ^ d) + 0x6ED9EBA1;
                    }
                    else {
                    if (i < 60)
                        e += (((b | c) & d) | (b & c)) + 0x8F1BBCDC;
                    else
                        e += (b ^ c ^ d) + 0xCA62C1D6;
                    }
                    e += w[i & 15] + rol(a, 5);
                    b = rol(b, 30);
                }
                for (int i = 0; i < 5; i++) h0[i] += h[i];
            }
            const char* base64tb = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            uint32_t triples[7] = {h0[0] >> 8,
                                (h0[0] << 16) | (h0[1] >> 16),
                                (h0[1] << 8) | (h0[2] >> 24),
                                h0[2],
                                h0[3] >> 8,
                                (h0[3] << 16) | (h0[4] >> 16),
                                h0[4] << 8};
            for (uint32_t i = 0; i < 7; i++) {
            out[i * 4] = base64tb[(triples[i] >> 18) & 63];
            out[i * 4 + 1] = base64tb[(triples[i] >> 12) & 63];
            out[i * 4 + 2] = base64tb[(triples[i] >> 6) & 63];
            out[i * 4 + 3] = base64tb[triples[i] & 63];
            }
            out[27] = '=';
            return 28;
        }
        /**
         * Create packet to send through WS
         *
         * @param dataPacket Payload data to send through WS
         * @param sizePacket Size of payload
         * @param wsPacket Output for WS Packet creation(must be sizePacket + 14bytes size)
         * @param fMasked Should we use mask in created WS packet
         * @param opCode OPCODE byte of Frame is constructed from (WS_FINFRAME and WS_OPCODE_?)
         * @return size of WS Packet stored in wsPacket, or less 0 if error
         */
        static int32_t createSendPacket(uint8_t *dataPacket, int32_t sizePacket, uint8_t* wsPacket, bool fMasked = true, uint8_t opCode = WS_OPCODE_BINFRAME | WS_FINFRAME) {
            //msg_dbg_call(("Crating packet for send size:%i mask:%i OPC:%i", sizePacket, fMasked, opCode)); // - TOO many calls

            int32_t iPWSl = 0;
            uint8_t uIMask[4];
            
            // First byte IS binary
            wsPacket[iPWSl++] = opCode;// Binary packet

            // Now store size of WS packet
            if(sizePacket < 126) {
                wsPacket[iPWSl++] = sizePacket | (fMasked ? 0x80 : 0);
            }
            else {
                if(sizePacket > 65535) {
                    wsPacket[iPWSl++] = 127 | (fMasked ? 0x80 : 0);
                    wsPacket[iPWSl++] = sizePacket >> 24;
                    wsPacket[iPWSl++] = sizePacket >> 16;
                    wsPacket[iPWSl++] = sizePacket >> 8;
                    wsPacket[iPWSl++] = sizePacket;
                }
                else {
                    wsPacket[iPWSl++] = 126 | (fMasked ? 0x80 : 0);
                    wsPacket[iPWSl++] = sizePacket >> 8;
                    wsPacket[iPWSl++] = sizePacket;
                }
            }

            if(fMasked) {
                // If we Masked -> generate random MASK
                uint32_t i32MASK;// Create non zero random
                i32MASK = rand();
                if(i32MASK == 0)i32MASK = 0x10dcd;
                wsPacket[iPWSl++] = uIMask[0] = i32MASK >> 24;
                wsPacket[iPWSl++] = uIMask[1] = i32MASK >> 16;
                wsPacket[iPWSl++] = uIMask[2] = i32MASK >> 8;
                wsPacket[iPWSl++] = uIMask[3] = i32MASK;

                // And store message
                for(int32_t iPCn = 0; iPCn < sizePacket; iPCn++) {
                    wsPacket[iPWSl++] = dataPacket[iPCn] ^ uIMask[iPCn & 0x03];
                }
            }
            else {// Just copy data to new place
                memcpy(wsPacket + iPWSl, dataPacket, sizePacket);
                iPWSl += sizePacket;
            }

            return iPWSl;
        }
        /**
         * Create response packet for PING message specificaly
         * 
         * @return Size of PONG message
         */
        static int32_t makePongMessage(uint8_t *pingBulk, int32_t bulkSize, uint8_t **pongOutput) {
            uint8_t uIMask[4];
            int32_t iPWSl = 0;
            uint32_t i32MASK;// Create non zero random
            // AS PING(less 125 bytes) got from server -> so NO MASK resend masked
            *pongOutput = new uint8_t[bulkSize + 4];// For MASK
            (*pongOutput)[iPWSl++] = 0x8A;// Pong FIN
            (*pongOutput)[iPWSl++] = 0x80 | pingBulk[1];// Same as PING but MASKED

            // Mask received message
            i32MASK = rand();
            if(i32MASK == 0)i32MASK = 0x10dcd;
            (*pongOutput)[iPWSl++] = uIMask[0] = i32MASK >> 24;
            (*pongOutput)[iPWSl++] = uIMask[1] = i32MASK >> 16;
            (*pongOutput)[iPWSl++] = uIMask[2] = i32MASK >> 8;
            (*pongOutput)[iPWSl++] = uIMask[3] = i32MASK;

            // Payload size
            bulkSize = pingBulk[1] & 0x7F;

            // And store message
            for(int32_t iPCn = 0; iPCn < bulkSize; iPCn++) {
                (*pongOutput)[iPWSl++] = pingBulk[iPCn] ^ uIMask[iPCn & 0x03];
            }

            return iPWSl;
        }
        /**
         * Get size of PONG message
         * 
         * @return Size of PONG message
         */
        static int32_t getPONGSize(uint8_t *dataBulk) {
            int32_t blkPos = 0;
            int32_t paySize;
            bool fMasked;

            blkPos++;// Skip OPCODE
            fMasked = (dataBulk[blkPos] & 0x80) != 0;// Store MASK flag
            paySize = dataBulk[blkPos++] & 0x07f;// Get payloadSize first byte
            if(paySize == 127) {
                paySize = (((int32_t)dataBulk[blkPos]) << 24) | 
                    (((int32_t)dataBulk[blkPos + 1]) << 16) | 
                    (((int32_t)dataBulk[blkPos + 2]) << 8) | 
                    ((int32_t)dataBulk[blkPos + 3]);
                blkPos += 4;
            }
            else {
                if(paySize == 126) {
                    paySize = (((int32_t)dataBulk[blkPos]) << 8) | ((int32_t)dataBulk[blkPos + 1]);
                    blkPos += 2;
                }
            }
            if(fMasked)blkPos += 4;// Skip MASK

            return blkPos + paySize;
        }
    };

    /**
     * Contain HTTP parameters from WS initiation request
     */
    struct authWSContext {
    public:
        std::string uriStr;
        std::map<std::string,std::string> reqParams;
    };

    /**
     * Setting for socket server
     */
    struct serverSettingsContext {
    public:
        /**
         * Delay in registering new input sockets
         */
        int32_t AcceptTimeout = 100;
        /**
         * Maximum stack to store in outbound messages.
         */
        int32_t SendTCPStack = 30000;
        int32_t WebSocketMaxBlockSize = 10000;
    };

    /**
     * Socket to ctack input messages (Single thread solution)
     */
    class webSocketReadDecoder {
    private:
#pragma region Internal variables
        std::list<uint8_t*> _buffersList;
        std::list<int32_t> _buffersSizesList;
        uint8_t _lastOpCode;
#pragma endregion

        public:
#pragma region Upload / Construct packet
        /**
         * Add FIRST received BULK from dataBulk to readed packet. Unmask and store last OPCODE
         * 
         * @param dataBulk Next BULK to add to readed packet
         * @param sizeBulk Size of received BULK
         * 
         * @return processed size of BULK(if more than one in single call -> less than sizeBulk), of Error code if less 0
        */
        int32_t addBulk(uint8_t *dataBulk, uint8_t sizeBulk) {
            msg_dbg_call(("Decoding bulk received on WS size:%i", sizeBulk));

            int32_t paySize, blkPos = 0;
            bool fMasked;
            uint8_t maskBytes[4];
            uint8_t *packPayload;
            
            // First check if size OK
            if(sizeBulk < 2) {
                msg_dbg_error(("Bulk cant be less than 2 byte!"));
                return -1;
            }

            // By bytes decoding
            this->_lastOpCode = dataBulk[blkPos++];// Store last OPCODE
            fMasked = (dataBulk[blkPos] & 0x80) != 0;// Store MASK flag
            paySize = dataBulk[blkPos++] & 0x07f;// Get payloadSize first byte
            if(paySize == 127) {
                paySize = (((int32_t)dataBulk[blkPos]) << 24) | 
                    (((int32_t)dataBulk[blkPos + 1]) << 16) | 
                    (((int32_t)dataBulk[blkPos + 2]) << 8) | 
                    ((int32_t)dataBulk[blkPos + 3]);
                blkPos += 4;
            }
            else {
                if(paySize == 126) {
                    paySize = (((int32_t)dataBulk[blkPos]) << 8) | ((int32_t)dataBulk[blkPos + 1]);
                    blkPos += 2;
                }
            }

            // Check if message received FULL
            if(paySize > sizeBulk) {
                msg_dbg_error(("Payload size declared more than received size! PL:%i", paySize));
                return -2;
            }

            // Store Data to internal buffer
            this->_buffersList.push_back(packPayload = new uint8_t[paySize]);
            this->_buffersSizesList.push_back(paySize);
            if(fMasked) {// If masked                
                maskBytes[0] = dataBulk[blkPos++];
                maskBytes[1] = dataBulk[blkPos++];
                maskBytes[2] = dataBulk[blkPos++];
                maskBytes[3] = dataBulk[blkPos++];

                dataBulk += blkPos;// Set to first payload byte
                for(int32_t iPPos = 0; iPPos < paySize; ++iPPos) {
                    packPayload[iPPos] = *dataBulk++ ^ maskBytes[iPPos & 0x03];
                }
            }
            else {// Not Masked -> Just COPY
                memcpy(packPayload, dataBulk + blkPos, paySize);
            }
            blkPos += paySize;

            return blkPos;
        }
        /**
         * Clear all bulks from memory
         */
        void clearBulk() {
            this->_lastOpCode = 0x7f;
            for(auto ptBuf = this->_buffersList.begin(); ptBuf != this->_buffersList.end(); ptBuf++) {
                delete *ptBuf;
            }
            this->_buffersList.clear();
            this->_buffersSizesList.clear();
        }
        /**
         * Get if Last bulk was FIN
         */
        bool isFINMsg() {
            return ((this->_lastOpCode & 0x80) != 0);
        }
#pragma endregion

#pragma region C & D
        webSocketReadDecoder() {
            msg_dbg_call(("create bulk %X", this));

            this->_lastOpCode = 0x7f;
        }
        ~webSocketReadDecoder() {
            msg_dbg_call(("delete bulk %X", this));

            this->clearBulk();
        }
#pragma endregion
    };
    
    class webStreamSocket {
    public:
#pragma region State / Result defines
        static const int32_t WSTATE_CREATED = 0;
        static const int32_t WSTATE_AUTH_RECEIVED = 1;
        static const int32_t WSTATE_CONNECTED = 2;
        static const int32_t WSTATE_CLOSED = 3;

        static const int32_t WSSEND_OK = 0;
        static const int32_t WSSEND_FAILCONNECTED = -1;
        static const int32_t WSSEND_IOCTLERROR = -2;
        static const int32_t WSSEND_STACKFILLED = -3;
        static const int32_t WSSEND_UNKNOWNERROR = -4;
#pragma endregion
    private:
#pragma region Internal variables
        sockaddr_in _sockAddr;
        int32_t _sockFD = -1; // -1 = not opened

        authWSContext _myAuth;        
        int32_t _iWState = WSTATE_CREATED;
        
        webSocketReadDecoder _recvMessage;

        void *_ptrOwner = NULL;
#pragma endregion
        
#pragma region Close functions
        /**
         * Close socket if One exist.
         *
         * @param reason String with closing reason
         */
        void closeRaw(const char *reason) {
            msg_dbg_call(("%s WS:%X", reason, this));

            if (this->_sockFD != -1) { // Save message and close connection
                close(this->_sockFD);
                this->_sockFD = -1; // Closed state
            }// Else does not matter -> store only REAL reason to close
        }
#pragma endregion

#pragma region Message handling support functions
        /**
         * Called by EPOOLIN to handle icomming data.
         *
         * @param dataIncome Will create buffer of needed to read size
         * @param dataSize Will be actually readed size / size of dataIncome
         * @return if more 0 -> READ_HANDLED_{X} will define message inside, if less -> error
         */        
        int32_t readMessage() {
            msg_dbg_call(("Reading bulk on socket %X", this));

            int32_t dataSize, datPTR = 0;
            uint8_t *dataIncome, *deleteData = NULL, *datSend;
            
            do {
                // Determine size in buffer
                if((datPTR = ioctl(this->_sockFD, FIONREAD, &dataSize)) < 0 ) {
                    msg_dbg_error(("Failed IOCTL on WS:%X to determine socket size! %i", this, datPTR));
                    break;
                }

                // Create buffer needed size
                deleteData = dataIncome = new uint8_t[dataSize];// deleted at function END
                dataSize = recv(this->_sockFD, dataIncome, dataSize, 0);
                if(dataSize < 0) {// this is error -> pass error outside
                    this->closeRaw("read error");
                    this->_iWState = WSTATE_CLOSED;
                    break;
                }
                if(dataSize == 0) {// Remote socket close -> 
                    this->closeRaw("remote close");
                    this->_iWState = WSTATE_CLOSED;
                    break;
                }

                // Check state and message, and decide action
                switch(this->_iWState) {
                case WSTATE_CREATED:// Socket just created -> first message -> goto
                    this->processNonWSMessage(std::string((char*)dataIncome));
                    if(this->_myAuth.uriStr.length() != 0) {// IS GET first request
                        this->_iWState = WSTATE_AUTH_RECEIVED;// Mark AUTH received

                        // Process accept WSocket
                        dataSize = this->onCheckWSAccept(this, this->_ptrOwner, &this->_myAuth);
                        if(this->answerAUTH(dataSize == 0) == 0) {// if OK set state CONNECTED
                            if(dataSize == 0)this->_iWState = WSTATE_CONNECTED;
                            else {
                                msg_dbg_warning(("Socket connection Declined WS:%X connect message :\r\n%s", this, dataIncome));
                                this->closeRaw("USER declined connection");
                                this->_iWState = WSTATE_CLOSED;
                            }
                        }
                        else {
                            msg_dbg_warning(("Socket WS:%X send response failed :\r\n%s", this, dataIncome));
                            this->closeRaw("Failed to send response");
                            this->_iWState = WSTATE_CLOSED;
                        }
                    }
                    else {// Some failed message -> switch to FAIL return FAIL
                        this->answerAUTH(false);// Send connection failed
                        this->closeRaw("AUTH failed");
                        this->_iWState = WSTATE_CLOSED;
                    }
                    break;
                case WSTATE_AUTH_RECEIVED:// A short-timed state as we call user to check if we accept WS
                    msg_dbg_error(("AUTH RECV (WS:%X) state on message - must not be as user accept or reject it!", this));
                    this->closeRaw("Wrong sequence of AUTH");
                    this->_iWState = WSTATE_CLOSED;
                    break;
                case WSTATE_CONNECTED:// We in connected state -> this is message -> receive, decrypt and call reader
                    while(dataSize != 0) {
                        switch(*dataIncome & 0x07f) {
                        case wsutility::WS_OPCODE_CONTFRAME:
                        case wsutility::WS_OPCODE_TEXTFRAME:
                        case wsutility::WS_OPCODE_BINFRAME:// For now handle as binary ALL
                            datPTR = this->_recvMessage.addBulk(dataIncome, dataSize);
                            if(this->_recvMessage.isFINMsg()) {// Here call onFullMessageReceive
                                // TODO : Create message composer function and realise onFullMessageReceive
                            }
                            else {
                                // TODO : Create message composer function and realise onPartlyMessageReceive
                            }
                            break;
                        case wsutility::WS_OPCODE_PINGFRAME:// Here we must send back PONG frame
                            datPTR = wsutility::makePongMessage(dataIncome, dataSize, &datSend);
                            send(this->_sockFD, datSend, dataSize, MSG_NOSIGNAL);// Synchronous send of PONG
                            delete datSend;// Clear created buffer
                            break;
                        case wsutility::WS_OPCODE_PONGFRAME:// No need to send -> BUT need to determin size to skip
                            datPTR = wsutility::getPONGSize(dataIncome);
                            break;
                        case wsutility::WS_OPCODE_CONCLOSEFRAME:// Grace closing of socket
                            datPTR = dataSize;// Just skip all as it MUST be last message
                            break;
                        }
                        dataIncome += datPTR;
                        dataSize -= datPTR;// To next unprocessed packet
                    }
                    break;
                }
            } while(false);

            if(deleteData != NULL)delete deleteData;
            return this->_iWState;
        }
        /**
         * Process messages after creation to setup WS connection or switch something.
         *
         * @param message GET string to connect WS
         * @return processed request size
         */
        int32_t processNonWSMessage(std::string message) {
            msg_dbg_call(("Received not-initialized message %X size:%i", this, message.length()));

            int32_t iNLPos, nextNLPos = -2;

            this->_myAuth.uriStr = "";// POSSIBLE end up here Many times
            this->_myAuth.reqParams.clear();
            
            // Analize by lines as it HTTP request should be
            while(nextNLPos != std::string::npos) {
                iNLPos = nextNLPos + 2;
                nextNLPos = message.find("\r\n", iNLPos);
                std::string cutLine = message.substr(iNLPos, nextNLPos == std::string::npos ? (message.length() - iNLPos) : nextNLPos - iNLPos);
                if(iNLPos == 0) {// First line -> get URI
                    int32_t iSkHTTP = cutLine.find("HTTP/");
                    if(iSkHTTP != std::string::npos)this->_myAuth.uriStr = cutLine.substr(4, iSkHTTP - 5);
                    else {
                        msg_dbg_error(("HTTP in first string not found -> assume error! S:\r\n%s", message.c_str()));
                        break;
                    }
                }
                else {// Every other Line
                    size_t delPos = cutLine.find_first_of(":");
                    if(delPos != std::string::npos) {// Store values in MAP
                        this->_myAuth.reqParams[cutLine.substr(0, delPos)] = cutLine.substr(delPos + 2);
                    }
                }
            }

            return iNLPos;
        }
        /**
         * Process AUTH param and answer them if needed.
         *
         * @param dataIncome Will create buffer of needed to read size
         * @param dataSize Will be actually readed size / size of dataIncome
         * @return 0 if send back AUTH accepted, not 0 oterwise
         */
        int32_t answerAUTH(bool wsAccept = true) {
            msg_dbg_call(("Answering AUTH request on socket %X", this));

            char resp[1024];
            int32_t resp_len;

            if(wsAccept) {
                // First check if key presented
                auto meKeyPtr = this->_myAuth.reqParams.find("Sec-WebSocket-Key");
                if(meKeyPtr == this->_myAuth.reqParams.end()) {// Not present -> error
                    msg_dbg_error(("Sec-WebSocket-Key not exist in %X", this));
                    return -1;
                }

                // Next create MAGIC string
                uint8_t magikStr[32 + 36 + 64];// Total of 130 bytes can be used in sha1base64
                uint8_t acceptKey[32];
                memcpy(magikStr, meKeyPtr->second.c_str(), meKeyPtr->second.length());
                memcpy(magikStr + meKeyPtr->second.length(), "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", 36);
                
                // Now create Accept Key
                acceptKey[wsutility::sha1base64(magikStr, meKeyPtr->second.length() + 36, (char*)acceptKey)] = 0;

                // Create response string
                resp_len = sprintf(resp, "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: "
                                "Upgrade\r\nSec-WebSocket-Accept: %s\r\n", acceptKey);
                // If exist protocol -> respond IT (Sec-WebSocket-Protocol)
                meKeyPtr = this->_myAuth.reqParams.find("Sec-WebSocket-Protocol");
                if(meKeyPtr != this->_myAuth.reqParams.end()) {// Not present -> error
                    msg_dbg_info(("Sec-WebSocket-Protocol exist in PT:%X -> respond it (%s)", this, meKeyPtr->second));
                    resp_len += sprintf(resp + resp_len, "Sec-WebSocket-Protocol: %s\r\n", meKeyPtr->second.c_str());
                }
                resp_len += sprintf(resp + resp_len, "\r\n");// NEEDED
            }
            else {
                resp_len = sprintf(resp, "HTTP/1.1 403 Forbidden\r\nSec-WebSocket-Version: 13\r\n");
            }

            // All created -> send to client
            msg_dbg_info(("Sending ACK of socket %X to %s:%i", this, inet_ntoa(this->_sockAddr.sin_addr), htons(this->_sockAddr.sin_port)));
            int32_t checkSend = send(this->_sockFD, resp, resp_len, MSG_NOSIGNAL);// Here we send synchronous
            return 0;
        }
#pragma endregion

#pragma region Dead-End handlers
        static int32_t onCheckAuthAcceptDE(webStreamSocket *sender, void *owner, authWSContext *authRecv) {
            msg_dbg_warning(("Called default AUTH check on WS:%X -> accept default", sender));
            return 0;
        }
        static void onCloseDE(webStreamSocket *sender, void *owner) {
            msg_dbg_warning(("Called default onClose on WS:%X -> do nothing. reason:%s", sender, ""));
        }
#pragma endregion
        
        // Hide default constructor
        webStreamSocket() {}
    public:
#pragma region Handlers of Events
        void setOwner(void *newOwner) {
            this->_ptrOwner = newOwner;
        }
        /**
         * Called on receiving AUTH Upgrade websocket
         * 
         * @param sender Source of called AUTH check
         * @param authRecv Authorization parameters 'Sec-WebSocket-Key' 'Sec-WebSocket-Protocol' etc
         * 
         * @return if 0 -> connection accepted if not 0 not accepted
         */
        int32_t (*onCheckWSAccept)(webStreamSocket *sender, void *owner, authWSContext *authRecv) = &onCheckAuthAcceptDE;
        /**
         * Called on closing frame and on close of FD
         * 
         * @param sender Source of called AUTH check
         * @param isClosedFD true mean socket FD is closed
         */
        void (*onWSClosedEvent)(webStreamSocket *sender, void *owner) = &onCloseDE;
#pragma endregion

#pragma region Call controlling functions
        /**
         * Connect to IP given in constructor.
         *
         * @param fReconnect Should close old connection
         * @return 0 if connection succesful. error code oterwise.
         */
        int32_t Connect(bool ServerSocket, bool fReconnect = false) {
            msg_dbg_call(("to %i WS:%X", ServerSocket, this));

            int32_t rVal;
            int flags;

            if (this->_sockFD != -1) { // Check if socket already connected
                if (fReconnect) { // Attempt reconnect
                    this->closeRaw("reconnection");
                }
                else { // Connected and No reconnect -> exit ALLOK
                    msg_dbg_warning(("Socket WS:%X connected and no reconnection Flag.", this));
                    return 0;
                }
            }

            // Create socket FD
            if ((this->_sockFD = socket(AF_INET, SOCK_STREAM, 0)) < 0) {// Default Socket for now
                rVal = this->_sockFD; // Store error
                this->_sockFD = -1;   // Mark socket closed
                msg_dbg_error(("Connect socket WS:%X Error %i", this, rVal));
                return rVal;
            }
            msg_dbg_info(("Socket WS:%X created %i", this, this->_sockFD));

            // Set access parameters -> Server/Client
            if (ServerSocket) {
                flags = fcntl(this->_sockFD, F_GETFL, 0); // Get current parameters
                if (fcntl(this->_sockFD, F_SETFL, flags | O_NONBLOCK) < 0) { // Add non block read/write
                    this->closeRaw("fcntl O_NONBLOCK error");
                    return -1;
                }
                // Allow BIND socket listening
                if (setsockopt(this->_sockFD, SOL_SOCKET, SO_REUSEADDR, &flags, sizeof(flags)) < 0) {
                    this->closeRaw("setsockopt SO_REUSEADDR error");
                    return -2;
                }
                // Bind server listening
                if (bind(this->_sockFD, (struct sockaddr *)&this->_sockAddr, sizeof(this->_sockAddr)) < 0) {
                    this->closeRaw("bind error");
                    return -3;
                }
                // Start listening socket
                if (listen(this->_sockFD, 5) < 0) {
                    this->closeRaw("listen error");
                    return -4;
                }
                msg_dbg_info(("Socket WS:%X listening %s:%i started", this, inet_ntoa(this->_sockAddr.sin_addr), htons(this->_sockAddr.sin_port)));
                return 0;
            }
            else {
                // Attempt connecting socket
                rVal = connect(this->_sockFD, (sockaddr *)&this->_sockAddr, sizeof(this->_sockAddr));
                if (rVal < 0) { // Connection failed -> drop socket wait NEW
                    msg_dbg_error(("Socket WS:%X connection failed %i", this, rVal));
                    this->closeRaw("Connection failed");
                }
                msg_dbg_info(("Socket WS:%X connected to %s:%i", this, inet_ntoa(this->_sockAddr.sin_addr), htons(this->_sockAddr.sin_port)));
                return rVal;
            }
        }
        /**
         * Calling accept on socket and return it
         *
         * @param addressOUT Incoming connection arrdess
         * @param lenOUT Size of address
         * @return Return value is return of accept()
         */
        int32_t AcceptCall(sockaddr *addressOUT, socklen_t *lenOUT) {
            //msg_dbg_call(("%X", this)); // - too many messages
            return accept(this->_sockFD, addressOUT, lenOUT);
        }
        /**
         * Start thread processing R/W/E events.
         *
         */
        void startRWEThread() {
            std::thread thWSRWProcessor = std::thread([this](){
                try {
                    msg_dbg_call(("thread of WS:%X FD:%i", this, this->_sockFD));

                    // As we have one socket - one thread -> use poll() NOT epoll()
                    struct pollfd meSockPoll;
                    
                    // Check if FD not -1 -> that socket created aand connected
                    if(this->_sockFD == -1) {
                        msg_dbg_error(("Socket WS:%X not created!"));
                        return;
                    }
                    
                    // Define this FD and handled events
                    meSockPoll.fd = this->_sockFD;
                    meSockPoll.events = POLLIN | POLLERR | POLLHUP;

                    // Nov operate on received messages
                    for(;;) {
                        int32_t iPWRet = poll(&meSockPoll, 1, 1000);
                        if (iPWRet < 0) { // Error in waiting
                            msg_dbg_error(("epoll_wait error %x", iPWRet));
                        }
                        else {
                            if (meSockPoll.revents & POLLIN) {
                                msg_dbg_info(("POLLIN event on FD:%X", this->_sockFD));
                                int32_t iProcRes = this->readMessage();
                                switch (iProcRes) {
                                case webStreamSocket::WSTATE_CREATED:
                                    msg_dbg_error(("MUSTNOTBE handled of WS:%X", this));
                                    break;
                                case webStreamSocket::WSTATE_AUTH_RECEIVED:
                                    msg_dbg_info(("AUTH message handled of WS:%X", this));
                                    break;
                                case webStreamSocket::WSTATE_CONNECTED:
                                    msg_dbg_info(("MESSAGE/PING handled of WS:%X", this));
                                    break;
                                case webStreamSocket::WSTATE_CLOSED:
                                    msg_dbg_info(("CLOSED of WS:%X", this));
                                    this->onWSClosedEvent(this, this->_ptrOwner);
                                    return;// On closed there is nothing to left but exit
                                }
                            }
                            if (meSockPoll.revents & POLLERR) {// Mean some error occured READ error handled in POLLIN, assume wirte error here
                                msg_dbg_info(("POLLERR event on FD:%X", this->_sockFD));
                                this->closeRaw("POLLERR detecded!");
                                this->onWSClosedEvent(this, this->_ptrOwner);
                                return;// Full close on error
                            }
                            if (meSockPoll.revents & POLLHUP) {// Mean connection is hanged -> exit
                                msg_dbg_info(("POLLHUP event on FD:%X", this->_sockFD));
                                this->closeRaw("Connection hangupped!");
                                this->onWSClosedEvent(this, this->_ptrOwner);
                                return;// Full close on hangup
                            }
                            meSockPoll.revents = 0;// Clear events flags
                        }
                    }
                }
                catch(char *thErr) {
                    this->closeRaw(thErr);
                }
            });
            thWSRWProcessor.detach();
        }
        /**
         * Send (none-blocking) data to WebSocket if there is space in stack.
         *
         * @param data Pointer to sending data
         * @param datSize Sending data size
         * @param iBufferedSize Maximum holded size in TCP stack(not sending if more occupied)
         * @return 0 - if sended OK. >0 if occupied. <0 if Error
         */
        int32_t sendIfFree(uint8_t *data, int32_t datSize, int32_t iBufferedSize = 30000) {
            int32_t datPTR;
            int32_t iErrRet;

            // First Try to Lock sendMutex
            if(this->_iWState != WSTATE_CONNECTED)return WSSEND_FAILCONNECTED;// Not received AUTH
            if((iErrRet = ioctl(this->_sockFD, TIOCOUTQ, &datPTR)) < 0 ) {
                msg_dbg_error(("Failed IOCTL on WS:%X to determine send buffer free! %i", this, iErrRet));
                return WSSEND_IOCTLERROR;
            }
            if(datPTR > iBufferedSize)return WSSEND_STACKFILLED;

            try {// Synchronously send
                send(this->_sockFD, data, datSize, MSG_NOSIGNAL | MSG_DONTWAIT);
                return WSSEND_OK;
            }
            catch (char *exMessage) {
                return WSSEND_UNKNOWNERROR;
            }
        }
#pragma endregion

#pragma region C & D
        /**
         * Create WEB Socket with defined target IP.
         *
         * @param address Target address to connect
         */
        webStreamSocket(sockaddr_in *address) {
            msg_dbg_call(("WS:%X", this));

            memcpy(&this->_sockAddr, address, sizeof(this->_sockAddr));
        }
        /**
         * Structure existing connection to WEB SOCKET.
         *
         * @param address Target address to connect
         * @param sockFD Connected socket FD
         */
        webStreamSocket(sockaddr_in *address, int32_t sockFD) {
            msg_dbg_call(("WS:%X", this));

            memcpy(&this->_sockAddr, address, sizeof(this->_sockAddr));
            this->_sockFD = sockFD; // Store existing connection
        }
        /**
         * Destructor of RAW Socket.
         */
        ~webStreamSocket() {
            msg_dbg_call(("WS:%X", this));

            this->closeRaw("destructor");
        }
#pragma endregion
    };
    /**
     * Class for sending given frames to all cliented socket.
    */
    class stream_ws_server {
    private:
#pragma region Internal Variables
        webStreamSocket *_serverSocket = NULL;
        serverSettingsContext _workSettings;
        std::list<webStreamSocket *> _sockWSList;
        std::mutex _muSockLock;

        std::thread _serverThread;
        int32_t _iThCloseSync = 0;
#pragma endregion

#pragma region Handling of socket events on server side
        static void onWSClosed(webStreamSocket *closedSocket, void *voidOwner) {
            ((stream_ws_server*)voidOwner)->removeSocket(closedSocket);
        }
#pragma endregion

#pragma region Socket list Control
        /**
         * Register all callbacks, start handling and store socket in list
         *
         * @param newSocket A socket to store - will be deleted in removeSocket
         */
        void addNewSocket(webStreamSocket *newSocket) {
            msg_dbg_call(("PT:%X WS:%X", this, newSocket));

            newSocket->setOwner(this);// To pass in every handlers
            newSocket->onWSClosedEvent = &onWSClosed;// Define closing handling

            this->_muSockLock.lock();// Must sync with every thread modifying/use sockets list
            this->_sockWSList.push_back(newSocket);
            newSocket->startRWEThread();// This create READ handling thread specific for this socket
            this->_muSockLock.unlock();
        }
        /**
         * Remove socket from List and call delete on IT
         *
         * @param sockToRemove A socket to remove and delete on IT
         */
        void removeSocket(webStreamSocket *sockToRemove) {
            msg_dbg_call(("PT:%X WS:%X", this, sockToRemove));

            this->_muSockLock.lock();
            auto sockInList = this->_sockWSList.begin();
            for(; sockInList != this->_sockWSList.end(); ++sockInList) {
                if(*sockInList == sockToRemove)break;// Search for value itself
            }
            if(sockInList != this->_sockWSList.end()) {// Found -> delete and remove
                delete *sockInList;// Delete webSocket
                this->_sockWSList.erase(sockInList);// Remove from dictionary
            }
            else {// Not found
                msg_dbg_error(("Element WS:%X not found in dictionary!", sockToRemove));
            }
            this->_muSockLock.unlock();
        }
#pragma endregion

#pragma region Server connection handlers
        /**
         * Cycle of accept socket
         *
         * @param flagToRun A sync flag pointer to stop cycle
         */
        void servCycle(int32_t *flagToRun = NULL) {
            msg_dbg_call(("PT:%X", this));

            bool localFlag = false;
            
            if (flagToRun == NULL) { // Use placeholder in case no external SYNC
                localFlag = true;
                flagToRun = new int32_t[1]; // Deleted at function end
                flagToRun[0] = 1;
            }
            try { // To terminate on error -> BUT TODO : Handle error message
                while (flagToRun[0] != 0) {
                    int32_t sockIN = -1;
                    sockaddr_in addressIN;
                    int32_t addLen = sizeof(addressIN);

                    // Check got new connection
                    if ((sockIN = this->_serverSocket->AcceptCall((sockaddr *)&addressIN, (socklen_t *)&addLen)) > 0) {
                        msg_dbg_info(("Socket PT:%X input received WS:%X from %s", this, sockIN, inet_ntoa(addressIN.sin_addr)));
                        // Adding socket to poll list
                        this->addNewSocket(new webStreamSocket(&addressIN, sockIN));// Deleted at onSocketClosing callback OR at destructor ~stream_ws_server
                    }
                    else std::this_thread::sleep_for(std::chrono::milliseconds(this->_workSettings.AcceptTimeout));
                }// TODO : define exit logic in case no external sync
            }
            catch (char *exMessage) {
            }
            if (localFlag) {
                delete flagToRun;
                flagToRun = NULL;
            }
        }
#pragma endregion

        /* hide default constructor */
        stream_ws_server() {}

    public:
#pragma region Server control
        /**
         * Set server options.
         *
         * @param settings Options for server
         * @return 0 if ALLOK
         */
        int32_t initServer(serverSettingsContext *settings) {
            msg_dbg_call(("PT:%X SET:%X", this, settings));

            memcpy(&this->_workSettings, settings, sizeof(serverSettingsContext));
            this->_serverSocket->Connect(true);
            return 0;
        }
        /**
         * Start server listening.
         *
         * @param fWaitEnding Should wait till server stop
         */
        void startServer(bool fSyncServer = true) {
            msg_dbg_call(("PT:%X  Sync:%i", this, fSyncServer));

            if (fSyncServer) { // Start server in this thread
                this->servCycle();
            }
            else { // Async launch of server Cycle
                if(this->_iThCloseSync == 0) {
                    this->_iThCloseSync = 1;
                    this->_serverThread = std::thread([this](){
                        this->servCycle(&this->_iThCloseSync);
                    });
                }
            }
        }
        /**
         * Set STOP flag and await to join thread.
         *
         * @param fWaitEnding Should wait till server stop
         */
        void stopServer(bool fWaitEnding) {
            this->_iThCloseSync = 0;
            if(fWaitEnding)this->_serverThread.join();
        }
        /**
         * Create a buffer wich will be readed and sended to all WS.
         *
         * @param msgData data to create buffer from
         * @param msgSize Size of sending data
         */
        void postBufferOnClients(uint8_t *msgData, int32_t msgSize) {
            uint8_t *wsSendBlock;// Local copy of sending buffer
            int32_t wsBlockSize = 0;
            int32_t iBlocksCount = 1 + (msgSize / this->_workSettings.WebSocketMaxBlockSize);
            int32_t iBn;
            
            // Create WS buffer to not waste in every thread
            wsSendBlock = new uint8_t[iBlocksCount * (this->_workSettings.WebSocketMaxBlockSize + 14)];
            for(iBn = 0; iBn < iBlocksCount - 1; iBn++) {
                wsBlockSize += wsutility::createSendPacket(msgData + (this->_workSettings.WebSocketMaxBlockSize * iBn), 
                    this->_workSettings.WebSocketMaxBlockSize, 
                    wsSendBlock + wsBlockSize, 
                    false, 
                    iBn == 0 ? wsutility::WS_OPCODE_BINFRAME : 0);
            }
            int32_t iRemSize = msgSize - (this->_workSettings.WebSocketMaxBlockSize * iBn);
            wsBlockSize += wsutility::createSendPacket(msgData + (this->_workSettings.WebSocketMaxBlockSize * iBn), iRemSize, wsSendBlock + wsBlockSize, false, 
                iBn == 0 ? wsutility::WS_FINFRAME | wsutility::WS_OPCODE_BINFRAME : wsutility::WS_FINFRAME);

            try {
                this->_muSockLock.lock();// Lock sockets on sending
                for(auto clPtr = this->_sockWSList.begin(); clPtr != this->_sockWSList.end(); clPtr++) {
                    switch((*clPtr)->sendIfFree(wsSendBlock, wsBlockSize, this->_workSettings.SendTCPStack)) {
                        case webStreamSocket::WSSEND_OK:
                            break;
                        case webStreamSocket::WSSEND_FAILCONNECTED:
                            msg_dbg_error(("Send frame to WS:%X RES:Not connected Size:%i", (*clPtr), wsBlockSize));
                            break;
                        case webStreamSocket::WSSEND_IOCTLERROR:
                            msg_dbg_error(("Send frame to WS:%X RES:IOCTL Err Size:%i", (*clPtr), wsBlockSize));
                            break;
                        case webStreamSocket::WSSEND_STACKFILLED:
                            msg_dbg_error(("Send frame to WS:%X RES:Stack Full Size:%i", (*clPtr), wsBlockSize));
                            break;
                        case webStreamSocket::WSSEND_UNKNOWNERROR:
                            msg_dbg_error(("Send frame to WS:%X RES:Unknown Error Size:%i", (*clPtr), wsBlockSize));
                            break;
                    }
                }
                this->_muSockLock.unlock();// Release sockets
            }
            catch(char *ErrMsg) {// To not forget created data                
            }

            // Free used memory
            delete wsSendBlock;
        }
#pragma endregion

#pragma region C & D
        /**
         * Create server and setup Server socket.
         *
         * @param ServerPort Incomming connection PORT
         * @param ServerAddress if NULL - use local IP, use given oterwise
         */
        stream_ws_server(uint16_t ServerPort, char *ServerAddress = NULL) {
            msg_dbg_call(("PT%X", this));

            bool fUseAutoAdr = false;
            sockaddr_in servIP;
            
            if (ServerAddress == NULL) { // Use local adress
                fUseAutoAdr = true;
                ServerAddress = new char[INET_ADDRSTRLEN]; // Deleted at function end
                if (wsutility::getLocalIPAddr(ServerAddress) != 0) {
                    throw "Can't get local IP address and no address specified -> throw EX!";
                }
            }

            msg_dbg_info(("Using listen socket - %s:%i", ServerAddress, ServerPort));

            // Create server IP struct
            servIP.sin_family = AF_INET;
            inet_pton(AF_INET, ServerAddress, &(servIP.sin_addr));
            servIP.sin_port = htons(ServerPort);
            for (int iZn = 0; iZn < 8; iZn++)
                servIP.sin_zero[iZn] = 0;

            // Create socket of server
            this->_serverSocket = new webStreamSocket(&servIP); // Deleted at destructor ~stream_ws_server

            if (fUseAutoAdr) { // Clear if we make it
                delete ServerAddress;
                ServerAddress = NULL;
            }
        }
        ~stream_ws_server() {
            msg_dbg_call(("PT:%X", this));

            if (this->_serverSocket != NULL) {
                delete this->_serverSocket;
                this->_serverSocket = NULL;
            }
            for (auto pkElem = this->_sockWSList.begin(); pkElem != this->_sockWSList.end(); pkElem++) {
                delete *pkElem; // Need to delete based on rawClass -> implement clear at destructor
            }
        }
#pragma endregion
    };
}