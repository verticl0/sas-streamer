package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"syscall"
	"time"

	"sastreamer/daemon"
	"sastreamer/httphandlers"
	"sastreamer/structs"
)

var (
	signal = flag.String("s", "", `Send signal to the daemon:
	quit — graceful shutdown
	stop — fast shutdown
	reload — reloading the configuration file`)
	dmninpcall = make(chan *structs.DaemonExchangeContext)
	dmnclsres  = make(chan *structs.DaemonExchangeContext)
)

func main() {
	flag.Parse()
	daemon.AddCommand(daemon.StringFlag(signal, "quit"), syscall.SIGQUIT, quitHandler)
	daemon.AddCommand(daemon.StringFlag(signal, "stop"), syscall.SIGTERM, termHandler)
	daemon.AddCommand(daemon.StringFlag(signal, "reload"), syscall.SIGHUP, hupHandler)

	sastreamerDaemon := &daemon.Context{
		PidFileName: "sastreamer.pid",
		PidFilePerm: 0644,
		LogFileName: "sastreamer.log",
		LogFilePerm: 0640,
		WorkDir:     "/home/sasstreamer/sas-streamer",
		Umask:       027,
		Args:        []string{},
	}

	if len(daemon.ActiveFlags()) > 0 {
		d, err := sastreamerDaemon.Search()
		if err != nil {
			log.Fatalf("Unable send signal to the daemon: %s", err.Error())
		}
		daemon.SendCommands(d)
		return
	}

	d, err := sastreamerDaemon.Reborn()
	if err != nil {
		log.Fatalln(err)
	}
	if d != nil {
		return
	}
	defer sastreamerDaemon.Release()

	log.Println("Starting HTTP handler of sastreamer")

	go httpHandlerMain() // Wich is handling HTTP request
	go exchRoutine()     // Wich will reload all settings/data/execute task

	err = daemon.ServeSignals()
	if err != nil {
		log.Printf("Error: %s", err.Error())
	}

	log.Println("Daemon of sastreamer stopped")
}

func httpHandlerMain() {
	http.Handle("/", httphandlers.GetNamedHTML(filepath.Join("htmlviews", "main.html")))
	http.Handle("/camList", httphandlers.GetCamListHTML(filepath.Join("htmlviews", "camlist.html")))
	http.Handle("/teststream", httphandlers.GetNamedHTML(filepath.Join("htmlviews", "teststream.html")))
	http.Handle("/testmjpeg", httphandlers.GetTestMJPEGHTML(filepath.Join("htmlviews", "testmjpeg.html")))
	http.Handle("/jscripts/", httphandlers.GetJScriptDispatcher("jscripts", http.FileServer(http.Dir(""))))
	log.Fatal(http.ListenAndServe(":8080", nil)) // This is blocking call -> so MUST be in async thread
}

func exchRoutine() {
EXCHLOOP:
	for {
		select {
		case inpData := <-dmninpcall:
			if inpData.IsClose {
				break EXCHLOOP
			}
		default:
			time.Sleep(350 * time.Millisecond)
		}
	}
	dmnclsres <- &structs.DaemonExchangeContext{}
}

func quitHandler(sig os.Signal) error {
	log.Println("QUIT called")
	dmninpcall <- &structs.DaemonExchangeContext{IsClose: true}
	<-dmnclsres // Await result/process if needed
	return daemon.ErrStop
}

func termHandler(sig os.Signal) error {
	log.Println("TERM called")
	dmninpcall <- &structs.DaemonExchangeContext{IsClose: true}
	return daemon.ErrStop
}

func hupHandler(sig os.Signal) error {
	log.Println("HUP called")
	return nil
}
